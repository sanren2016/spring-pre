package com.laolang.km.modules.admin.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequestMapping("admin/sys")
@Controller
public class SysIndexController {

	@RequestMapping(value = { "", "/", "/index" }, method = RequestMethod.GET, produces = "text/html")
	public ModelAndView index(ModelAndView mv) {
		log.info("admin sys index");
		mv.addObject("info", "spre mvc hello");
		mv.setViewName("/modules/admin/index");
		return mv;
	}

}
